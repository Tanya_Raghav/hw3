import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Container(
                  padding: const EdgeInsets.only(bottom: 12),
                  child: Text(
                    'A SKY FULL OF STARS',
                    style: TextStyle(
                      color: Colors.yellow,

                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'COLDPLAY',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          MyStatefulWidget(),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Cause you are a sky, cause you are a sky full of stars'
            'I am gonna give you my heart'
            'Cause you are a sky, cause you are a sky full of stars'
            'Cause you light up the path',
        style: TextStyle(
          color: Colors.white,
        ),
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Homework 3',
      home: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text('COLDPLAY SONGS'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'lib/image1.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            titleSection,
            textSection,
            ToggleButtonsExample(),
          ],
        ),
      ),
    );
  }
}
class ToggleButtonsExample extends StatefulWidget {
  @override
  ToggleButtonsExampleState createState() => new ToggleButtonsExampleState();
}

class ToggleButtonsExampleState extends State<ToggleButtonsExample> {
  List<bool> _isSelected = [false, true, false];

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ToggleButtons(
          children: <Widget>[
            Icon(Icons.bluetooth),
            Icon(Icons.share),
            Icon(Icons.file_download),
          ],
          isSelected: _isSelected,
          onPressed: (int index) {
            setState(() {
              _isSelected[index] = !_isSelected[index];
            });
          },
          // region example 1
          color: Colors.grey,
          selectedColor: Colors.yellow,
          fillColor: Colors.black,
          // endregion
          // region example 2
          borderColor: Colors.yellowAccent,
          selectedBorderColor: Colors.black,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          // endregion
        ),
      ],
    );
  }
}
int _volume = 0;
class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {

  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.volume_up),
          color: Colors.white,
          tooltip: 'Increase volume by 10',
          onPressed: () {
            setState(() {
              _volume += 10;
            });
          },
        ),
        Text('Volume : $_volume',
          style: TextStyle(
            color: Colors.white,
          ),
        ),

      ],
    );

  }
}
